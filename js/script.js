class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }

  get age() {
    return this._age;
  }

  set salary(value) {
    this._salary = value;
  }

  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value * 3;
  }
}

const programmer1 = new Programmer("Karina", 24, 1000, "uk");
const programmer2 = new Programmer("Marina", 29, 2000, "ru");
const programmer3 = new Programmer("Ivan", 34, 4000, "ab");
const programmer4 = new Programmer("Olga", 23, 5000, "en");
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);
